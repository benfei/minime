function [tbl_cuts] = numericcut(tbl, varname, values)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% Input validation
if ~istable(tbl)
    error('input:table', 'tbl must be a table');
end;

if ~ismember(varname, tbl.Properties.VariableNames)
    error('input:varname', 'var must be a variable name in tbl');
end;

if ~(ismatrix(values) && isnumeric(values))
    error('input:matrix', 'values must be a numeric matrix');
end;

if size(values,2) ~= 2
    error('input:matrix:size', 'values must be a nx2 matrix');
end;

%% Cutting
n_cuts = size(values, 1);

tbl_cuts = cell(n_cuts, 1);
for k = 1:n_cuts
    tbl_cuts{k} = tbl(values(k, 1) <= tbl.(varname) ...
        & tbl.(varname) <= values(k, 2), :);
end;

end

