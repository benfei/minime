function [out_tbl] = splitapply(tbl, bulk_size, varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if ~istable(tbl)
    error('input:table', 'tbl must be a table');
end;
if ~isnumeric(bulk_size)
    error('input:int', 'bulk_size must be a positive integer');
end;
bulk_size = floor(bulk_size);
if bulk_size < 1
    error('input:int', 'bulk_size must be a positive integer');
end;
p = inputParser;
addOptional(p, 'func', @mean, @(fh) isa(fh,'function_handle'));
addParameter(p, 'KeepHeaders', 0, @(x) x == 0 || x == 1 || islogical(x));
parse(p, varargin{:});
func = p.Results.func;
keep_headers = p.Results.KeepHeaders;
original_headers = tbl.Properties.VariableNames;
tbl.bulkmean__group__ = floor([0:height(tbl)-1]' / bulk_size);
out_tbl = varfun(func, tbl, 'GroupingVariables','bulkmean__group__');
out_tbl = out_tbl(out_tbl.GroupCount >= bulk_size / 3, 2:end);
if keep_headers
    varnames = out_tbl.Properties.VariableNames;
    varnames(2:end) = original_headers;
    out_tbl.Properties.VariableNames = varnames;
end;
out_tbl.Properties.RowNames = {};
end