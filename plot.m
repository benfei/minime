function [fig] = plot(tbls, varargin)
%PLOT Plot some tables on a single figure.
%   This methods gets a table or an array of tables,
%   along with a varname for the x-axis and an array of varnames
%   vor the y-axis for every table it gets.
%   The method plots this figures, one by one, according to the
%   order of the tables in the array and the order of the y-variables.
%   It returns the figure id in which the plot is.


%% Parse Input and Input Validation

if istable(tbls)
    tbls = {tbls};
end;
n_tbls = length(tbls);
tbls = reshape(tbls, 1, n_tbls);
for k = 1:n_tbls
    if ~istable(tbls{k})
        error('input:table', 'tbls{%d} is not a table', k);
    end;
end;
if length(varargin) < 2 * n_tbls
    error('input:varargin:size', ...
        'myplot must receive x-axis and y-axis for every given table');
end;

p = inputParser;
for k = 1:n_tbls
    addRequired(p, ['x', num2str(k)], @ischar);
    addRequired(p, ['y', num2str(k)], @(v) iscell(v) || ischar(v));
end;
addParameter(p, 'XLog', false, @(x) x == 0 || x == 1 || islogical(x));
addParameter(p, 'YLog', false, @(x) x == 0 || x == 1 || islogical(x));
parse(p, varargin{:});

x_vars = varargin(1:2:2*n_tbls);
y_vars = varargin(2:2:2*n_tbls);
for k = 1:length(y_vars)
    if ~iscell(y_vars{k})
        y_vars{k} = y_vars(k);
    end;
end;

% input validation
for k = 1:n_tbls
    if ~ismember(x_vars{k}, tbls{k}.Properties.VariableNames)
        error('input:varname', ...
            '%s is not a varname in table number %d', x_vars{k}, k);
    end;
    for j = 1:length(y_vars{k})
        if ~ismember(y_vars{k}{j}, tbls{k}.Properties.VariableNames)
            error('input:varname', ...
                '%s is not a varname in table number %d', y_vars{k}{j}, k);
        end;
    end;
end;

xlog = p.Results.XLog;
ylog = p.Results.YLog;

%% Plot
%%% Generate colors and labels
n_lines = sum(cellfun(@length, y_vars));
clrs = lines(n_lines);

x_lbl = strjoin(x_vars, ', ');
y_lbl = strjoin(cellfun(@(v) strjoin(v, ', '), ...
    y_vars, 'UniformOutput', false), ', ');
fig_title = [y_lbl, ' vs. ', x_lbl];

%%% Generate graphic elements
fig = figure;
ax = gca;
grid(ax, 'on');
hold(ax, 'on');
xlabel(ax, x_lbl);
ylabel(ax, y_lbl);
title(ax, fig_title);

if xlog
    ax.XScale = 'log';
end;
if ylog
    ax.YScale = 'log';
end;

clr_k = 1;
for k = 1:n_tbls
    tbl = tbls{k};
    x_var = x_vars{k};
    tbl = sortrows(tbl, x_var);
    for m = 1:length(y_vars{k})
        y_var = y_vars{k}{m};
        plot(gca, tbl.(x_var), tbl.(y_var),...
            'LineStyle', '-', 'Color', clrs(clr_k,:),...
            'DisplayName', y_var);
        clr_k = clr_k + 1;
    end;
end;
legend(ax, 'show');
hold(ax, 'off');

end