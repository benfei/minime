function [tbls] = readtable(path)
%READTABLE Reads table from an acceptable-format file, and returns it.
%   It may get the file's path as an argument, otherwise it'll open
%   ui dialog box for selecting file.
if nargin < 1
    [filename, folder] = uigetfile({'*.xls;*.xlsx;*.csv', 'Tables'}, 'Read Table');
    path = fullfile(folder, filename);
end;
[~,filename,~] = fileparts(path); 
tbls = readtable(path, 'Basic', 1);
tbls.Properties.Description = filename;
end