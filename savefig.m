function [] = savefig(fig, varargin)
%SAVEFIG Summary of this function goes here
%   Detailed explanation goes here
%% Input Validation
if ~ishandle(fig) || ~isa(handle(fig), 'matlab.ui.Figure')
    error('input:fig', 'fig must be a valid matlab.ui.Figure handle');
end;

p = inputParser;
addParameter(p, 'Path', '', @ischar);
addParameter(p, 'Extensions', {'png', 'bmp', 'eps'}, @iscell);
parse(p, varargin{:});

path = p.Results.Path;
if isempty(path)
    [filename, foldername] = uiputfile({'*.fig'});
    path = fullfile(foldername, filename);
end;


extentsions = p.Results.Extensions;
extentsions = reshape(extentsions, 1, length(extentsions));

% check if compact save is supported by this version of matlab (>= R2014b)
myver = version;
iscompact = str2double(myver(1:3)) >= 8.4;

%% Save Figures
if iscompact
    savefig(fig, path, 'compact');
else
    savefig(fig, path);
end;

path = path(1:end-4);
for ext = extentsions
    pathext = [path, '.', ext{1}];
    saveas(fig, pathext);
end;
end