function [df_dx] = derive(tbl, x, f)
%DERIVE Discrete derivative of a function in a table.
%   The method receives a table, the name of the x-variable, and the name
%   of the f-variable (s.t. our function is f(x)) and returns a vector of
%   f'(x) approx values.

%% Input Validation
if ~istable(tbl)
    error('input:table', 'tbl must be a table');
end;
if ~ismember(x, tbl.Properties.VariableNames)
    error('input:varname', 'x must be a variable name in tbl');
end;
if ~ismember(f, tbl.Properties.VariableNames)
    error('input:varname', 'f must be a variable name in tbl');
end;

%% Derive
x = tbl.(x);
f = tbl.(f);
dx = x(3:end) - x(1:end-2);
dx(dx == 0) = NaN;  % use NaN for undefined derivative values.
df = f(3:end) - f(1:end-2);
df_dx = [NaN; df ./ dx; NaN];
end