function [Qg_tbl] = calc_Qg(tbl, V, no_pump_times, dn, p_varname)
%CALC_QG Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    no_pump_times = [tbl.t(1), tbl.t(end)];
end;
if nargin < 4
    dn = 30;
end;
if nargin < 5
    p_varname = 'P_c';
end;
dpdt_varname = ['d', p_varname, 'dt'];
tbls = minime.numericcut(tbl, 't', no_pump_times);
tbls_chunks = cellfun(@(T) minime.splitapply(T, dn), tbls, 'UniformOutput', false);

t_s = no_pump_times(:, 1);
t_e = no_pump_times(:, 2);

dPdt = cellfun(@(T) mean(T.(['mean_', dpdt_varname])), tbls_chunks);
dPdterr = cellfun(@(T) std(T.(['mean_', dpdt_varname])), tbls_chunks);

P = cellfun(@(T) mean(T.(['mean_', p_varname])), tbls_chunks);
Perr = cellfun(@(T) std(T.(['mean_', p_varname])), tbls_chunks);

Qg = V * dPdt;
Qgerr = V * dPdterr;

Sg = Qg ./ P;
Sgerr = Sg .* sqrt((Qgerr./Qg).^2 + (Perr./P).^2);

Qg_tbl = table(t_s, t_e, P, Perr, dPdt, dPdterr, Qg, Qgerr, Sg, Sgerr);
end