function [Q, S] = sysprops(tbl, V, p_varname, Qg_varname)
%SP Summary of this function goes here
%   Detailed explanation goes here

if nargin < 4
    p_varname = 'P_c';
end;

if nargin < 5
    Qg_varname = 'Qg';
end;

dpdt_varname = ['d', p_varname, 'dt'];
Q = -V * tbl.(dpdt_varname) - tbl.(Qg_varname);
S = Q ./ tbl.(p_varname);
end
