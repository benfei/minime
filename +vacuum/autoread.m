function [tbl, f1, f_var] = autoread(p_varnames, varargin)
%AUTOREAD Summary of this function goes here
%   Detailed explanation goes here

%% Input
if ischar(p_varnames)
    p_varnames = {p_varnames};
end;
if ~iscell(p_varnames)
    error('p_varnames must be a string or a cell array');
end;

%% Read
tbl = minime.readtable(varargin{:});

%% Derivatives
dpdt_varnames = cell(size(p_varnames));
for k = 1:numel(p_varnames)
    dpdt_varnames{k} = ['d', p_varnames{k}, 'dt'];
    tbl.(dpdt_varnames{k}) = minime.derive(tbl, 't', p_varnames{k});
end;
tbl = tbl(2:end-1, :);

%% Plot
f1 = 0;
f_var = 0;
%f1 = minime.plot(tbl, 't', p_varnames, 'YLog', 1);
%f_var = cell(1, numel(p_varnames));
%for k = 1:numel(p_varnames)
%    f_var{k} = minime.plot(tbl, 't', [p_varnames(k), dpdt_varnames(k)], 'YLog', 1);
%end;
end